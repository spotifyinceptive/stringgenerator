package se.inceptive;
/**
 * Created by peter on 19/10/15.
 */
public class Counterstring {
    public static String counterstring(int i) {
        if (i < 2){
            return "*";
        }
        else if (i == 3){
            return "*3*";
        }
        else if (i == 4){
            return "2*4*";
        }
        return "2*";
    }

    public static String counterstringRecursive(int i) {
        String ret = "";
        if (i < 1)
            return ret;
        int sub = (i + "*").length();
        ret = counterstringRecursive(i - sub) + i + "*";
        if (ret.length() > i)
            ret = ret.substring(1);
        return ret;
    }
}
