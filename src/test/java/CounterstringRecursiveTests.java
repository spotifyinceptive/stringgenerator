package se.inceptive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static se.inceptive.Counterstring.*;

/**
 * Created by peter on 20/10/15.
 */
@RunWith(Parameterized.class)
public class CounterstringRecursiveTests {
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {0, ""}, {1, "*"}, {2, "2*"}, {3, "*3*"}, {4, "2*4*"}, {5, "*3*5*"}, {10, "*3*5*7*10*"}, {11, "2*4*6*8*11*"}
        });
    }

    private int fInput;
    private String fExpected;

    public CounterstringRecursiveTests(int fInput, String fExpected) {
        this.fInput = fInput;
        this.fExpected = fExpected;
    }

    @Test
    public void testData() {
        Assert.assertEquals(counterstringRecursive(fInput), fExpected);
    }
}
