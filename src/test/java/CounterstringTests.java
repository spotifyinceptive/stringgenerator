package se.inceptive;
import org.junit.Assert;
import org.junit.Test;

import static se.inceptive.Counterstring.*;

/**
 * Created by peter on 19/10/15.
 */
public class CounterstringTests {
    @Test
    public void testBasic() {
        Assert.assertEquals("*", counterstring(1));
    }

    @Test
    public void testBasic2() {
        Assert.assertEquals("2*", counterstring(2));
    }

    @Test
    public void testBasic3() {
        Assert.assertEquals("*3*", counterstring(3));
    }

    @Test
    public void testBasic4() {
        Assert.assertEquals("2*4*", counterstring(4));
    }
}
